import React from 'react'

/**
 * Presentational component (Stateless function components).
 */
const SampleComponent = ({ textParameter }) => (
    <div>
        <p>{textParameter}</p>
    </div>
)

export default SampleComponent
