import React, { Component } from 'react'

import SampleComponent from '../component/sample-component'

/**
 * Container component (maintain state and provide data for presentational component)
 */
class SampleContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            sampleContainerState: 'Initialization of a default state value in component constructor.',
            users: [{ id: 12, name: 'Martin' }, { id: 15, name: 'Stéphane' }, { id: 17, name: 'Julie' }],
            selectedUser: undefined
        }
    }

    /**
     * Using fat arrow we don't need to use .bind(this) and we can use function currying.
     * Note: ESLint should use babel-eslint
     * @param {*} index
     * @returns
     */
     handleClick = (index) => () => {
        this.setState({
            selectedUser: Object.assign({}, this.state.users[index])
        })
    }

    render () {
        return (
            <div>
                <h1>Boilerplate Client React</h1>
                <SampleComponent textParameter={this.state.sampleContainerState} />

                <h2>User List</h2>
                <ul>
                    {this.state.users.map((user, index) => <li key={user.id} onClick={this.handleClick(index)}>{user.name}</li>)}
                </ul>

                <h2>Selected User</h2>
                {this.state.selectedUser && this.state.selectedUser.name}
            </div>
        )
    }
}

export default SampleContainer
