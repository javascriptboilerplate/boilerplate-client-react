# boilerplate-client-react

## Description

Minimal boilerplate for [ReactJs](https://reactjs.org/) client development using [Webpack](https://webpack.js.org/), [Babel](https://babeljs.io/) and [ESLint](https://eslint.org/).

## Recommanded tools

1. [Visual Studio Code](https://code.visualstudio.com/)
    1. EditorConfig for VS Code
    2. ESLint (Dirk Baeumer)

Note: Add the following settings in Visual Studio code to enable ESLint
```
  "eslint.autoFixOnSave": true,
  "javascript.format.enable": false,
  "editor.formatOnSave": true,
```

## Usage

1. For development: `npm run dev`

    1. The application will open in your default browser after being automatically compiled.
    2. The application will be compiled and reloaded automatically after code update.


2. For deployment: `npm run build`

    1. Produce a complete application packaging ready for deployment in the **/dist** folder.